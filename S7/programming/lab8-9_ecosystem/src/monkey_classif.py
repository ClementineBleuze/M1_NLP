import argparse
import collections
import operator as op

import pandas as pd
import numpy as np

from monkey_model import Monkey
from utils import check_hexacolor, hexacolor_to_int, euclidean_distance, get_cli_args, VALID_OBS, patch_color_int


def read_monkeys_from_csv(csv_filepath:str, strict:bool=False) -> pd.DataFrame:
    """Read a monkey data from a CSV file and produce and return a dataframe"""
    # Loading the dataset
    data = pd.read_csv(csv_filepath)
    EXPECTED_COLUMNS = set(["fur_color", "size", "weight", "species"])
    
    # Looking for missing values
    if strict and data.isnull().values.any():
        raise ValueError(f"CSV file {csv_filepath} has missing values.")
    
    # Checking the columns
    if set(data.columns) != EXPECTED_COLUMNS:
        raise ValueError(f"CSV file {csv_filepath} should contain the following columns: {EXPECTED_COLUMNS}.")
    
    if not len(data):
        return data.reindex(columns=["fur_color","size","weight","species","monkey","fur_color_int","bmi"])
    
    # Dropping missing values and invalid rows
    data = data.dropna(axis=0, subset=["size", "weight", "fur_color"])
    data = data[data["size"] > 0]
    data = data[data["weight"] > 0]
    
    bad_index = []    # Wrong hexadecimal codes
    for i in data.index:
        if not(check_hexacolor(data["fur_color"][i])):
            bad_index.append(i)
    data.drop(bad_index, inplace = True)
    
    # Adding Monkeys
    monkeys = data.apply(lambda x: Monkey(round(x["weight"],3), round(x["size"],3), x["fur_color"], x["species"]), axis=1)
    data["monkey"] = monkeys
    
    # Converting hexadecimal colors into integers
    data["fur_color_int"] = [hexacolor_to_int(hexc) for hexc in data["fur_color"]]
    
    # Adding BMI
    data["bmi"]= [mk.compute_bmi() for mk in data["monkey"]]
    return data


def compute_knn(df:pd.DataFrame, k:int=5, columns:list=["fur_color_int", "bmi"])->pd.DataFrame:
    """update species information for a Monkey DataFrame using a KNN.
    Arguments:
        `df`: dataframe as obtained from `read_monkeys_from_csv`
        `k`: number of neighbors to consider
        `columns`: list of observations to consider. Are valid observations:
            - fur_color_int,
            - fur_color_int_r (for red hue of fur),
            - fur_color_int_g (for green hue of fur),
            - fur_color_int_b (for blue hue of fur),
            - weight
            - size
            - bmi
    Returns: the dataframe `df`, modified in-place
    """
    
    unlabelled_monkeys = [df.index[i] for i in range(len(df.index)) if df["species"][df.index[i]] not in ["gorilla", "orangutan", "bonobo"]] # finding Monkeys with empty species attribute
    labelled_monkeys = [df.index[i] for i in range(len(df.index)) if df.index[i] not in unlabelled_monkeys] # all the other Monkeys are labelled
    
    for i in range(len(df.index)):
        index_i = df.index[i]
        
        if index_i in unlabelled_monkeys: # for each unlabelled Monkey I
            coords_i = [df[columns[1]][index_i], df[columns[0]][index_i]]
            neighbors = []
                           
            for index_j in labelled_monkeys: # for each labelled Monkey J
                coords_j = [df[columns[1]][index_j], df[columns[0]][index_j]]
                distance = euclidean_distance(coords_i, coords_j)  # compute the distance between Monkeys I and J
                
                if len(neighbors)<k:     # if we don't have k neighbors yet, just select the Monkey as a neighbor
                    neighbors.append((index_j, distance))
                               
                else:                    # else, check if the Monkey is closer than the current neighbors
                    furthest_nb = max(neighbors, key = op.itemgetter(1)) # the furthest neighbor
                    if distance < furthest_nb[1]:
                        neighbors.remove(furthest_nb)
                        neighbors.append((index_j, distance))
            
            species = [df["species"][nb[0]] for nb in neighbors]
            # Finding the majority species in the neighbors
            if species:
                label = max( [(sp, species.count(sp)) for sp in species] , key = op.itemgetter(1) )[0]
            else:
                label = "still unknown"
            
            # Updating data
            df["species"][index_i] = label
            df["monkey"][index_i] = Monkey(round(df["weight"][index_i],3), round(df["size"][index_i],3), df["fur_color"][index_i], label)
           
    return df


def save_to_csv(dataframe:pd.DataFrame, csv_filename:str):
    """Save monkey dataframe to CSV file"""
    dataframe.drop(columns=["monkey", "fur_color_int", "bmi"]).to_csv(csv_filename, index=False)


def main():
    args = get_cli_args()
    if args.command == "knn":
        df = read_monkeys_from_csv(args.input_csv)
        df = compute_knn(df, k=args.k, columns=args.obs)
        save_to_csv(df, args.output_csv)
    elif args.command == "visualize":
        from monkey_visualize import scatter
        scatter(args.input_csv, args.obs_a, args.obs_b)
    else:
        # this should be dead code.
        raise RuntimeError("invalid command name")


# main entry point
if __name__ == "__main__":
    main()
