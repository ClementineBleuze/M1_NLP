import unittest

class MyTest(unittest.TestCase):
    
    def test_print_everything(self):
        self.assertEqual(print_everything(1,2),"1,2","Should be '1,2'")
    
    def test_client(self):
        c = Client("Bob",1234)
        self.assertEqual(c.name,"Bob","Should be 'Bob'")

if __name__ == "__main__":
    unittest.main()
    print("ok")