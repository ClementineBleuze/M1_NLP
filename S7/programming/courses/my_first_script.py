def greet(name):
    print("Hello {name}, I'm Python !".format(name=name))

if __name__=="__main__":
    name = input("What's your name ?\n")
    greet(name)
